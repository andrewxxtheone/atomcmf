<?php

require_once "./Application/UniversalClassLoader.php";

$loader = new UniversalClassLoader();
$loader->registerNamespaceFallbacks(array("./Packages"));
$loader->registerPrefixFallbacks(array("./Vendors", "./Vendors/Sass"));
$loader->register(); 