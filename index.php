<?php
//ini_set('display_errors', '0'); 
error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE & ~E_STRICT);

require_once "./Application/Autoloader.php";

class Application extends \Atom\Core\Kernel {
	
}

$application = new Application();
$application->handle(\Atom\HttpKernel\Request::createFromGlobals());

//Smartgit and Sublime Text 2 test