<?php

namespace Atom\HttpKernel;

/**
 * undocumented class
 *
 * @package default
 * @author  
 */
class Response {
	
	/**
	 * undocumented class variable
	 *
	 * @var string
	 */
	var $content;
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author  
	 */
	function __construct($content) {
		$this->content = $content;
	}
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author  
	 */
	function display() {
		echo $this->content;
	}
	
} // END
