<?php

namespace Atom\HttpKernel;

use
	Atom\Core\ResourceMgr;

/**
 * This little class, responsible for all the routings
 *
 * @package Atom\HttpKernel
 * @author  Daniel Simon
 */
class Router {
	
	/**
	 * Routes
	 *
	 * @staticvar array Routes defined in Router.yaml
	 */
	static $routes;
	
	/**
	 * Mostly just reads out the Router.yaml file...
	 *
	 * @return void
	 * @author  Daniel Simon
	 */
	static function initialize() {
		if(is_array(self::$routes))
			return;
		$resmgr = new ResourceMgr();
		self::$routes = $resmgr->getConfig("Router");
	}
	
	/**
	 * Lookup all the routes and decides what fits the most to the given url
	 *
	 * @return array Route's array
	 * @author  Daniel Simon
	 * @param string Url...
	 */
	static function getAction($url) {
		self::initialize();
		foreach(self::$routes as $route_name => $route) {
			$url_pattern = self::buildUrlPattern($route['url'], array_key_exists('rules', $route)?$route['rules']:array());
			if(preg_match($url_pattern, $url, $matches) == 1) {
				$arguments = array();
				foreach($matches as $key => $value) {
					if(!is_numeric($key)) {
						if($value == "" && !array_key_exists($key, $route["action"]))
							continue 2;
						$arguments[$key] = (($value == "")?$route["action"][$key]:$value);
					}
				}
				//lets build return array
				return array(
					"route_name" => $route_name,
					"action" => $route["action"]["_controller"],
					"arguments" => $arguments
				);
			}
		}
	}
	
	/**
	 * Creates a regexp pattern from raw_url_pattern and rules
	 *
	 * @return string Reqexp pattern built from raw_url_pattern from router.yaml and rules
	 * @author  Daniel Simon
	 */
	static function buildUrlPattern($url_raw_pattern, $rules) {
		preg_match_all("/\{(?<key>[a-zA-Z_]*)\}/", $url_raw_pattern, $matches, PREG_SET_ORDER);
		foreach($matches as $match)
			$url_raw_pattern = str_replace("{".$match['key']."}", "(?<".$match['key'].">".(array_key_exists($match['key'], $rules)?$rules[$match['key']]:'.*').")", $url_raw_pattern);
		return "/".str_replace("/", "\/", $url_raw_pattern)."/";
	}
	
	/**
	 * Creates relative url
	 *
	 * @return string Relative url
	 * @author  Daniel Simon
	 */
	static function createUrl($key, $args) {
		foreach(self::$routes as $name => $route) {
			if($key == $name) {
				$url = $route['url'];
				foreach($args as $k => $v) {
					$url = str_replace("{".$k."}", $v, $url);
				}
				return substr($url, 1);
			}
		}
	}
	
} // END