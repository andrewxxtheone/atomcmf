<?php

namespace Atom\HttpKernel;

use
	Atom\Core\PackageMgr;

/**
 * Every request is held by an instance of this class
 * It can be used as perform requests to another webserver or receive request from clients
 *
 * @package Atom\HttpKernel
 * @author  Daniel Simon
 */
class Request {
	
	/**
	 * Holds the url of the performed action
	 *
	 * @var string Url
	 */
	var $url;
	
	/**
	 * Post datas of the request
	 *
	 * @var array Post datas
	 */
	var $post;
	
	/**
	 * Cookies of the request
	 *
	 * @var array Cookies
	 */
	var $cookies;
	
	/**
	 * Constructor of Request class
	 *
	 * @return void
	 * @author  Daniel Simon
	 * @param string Url
	 * @param array Post data
	 * @param array Cookies
	 */
	function __construct($url, $post = array(), $cookies = array()) {
		$this->url = $url;
		$this->post = $post;
		$this->cookies = $cookies;
	}
	
	/**
	 * Handles the request fetches the router for routing and involves PackageMgr as well as ResourceMgr
	 *
	 * @return \Atom\HttpKernel\Response A response object what is created by the called controller, holds headers and body
	 * @author  Daniel Simon
	 */
	function handle() {
		$route = Router::getAction($this->url);
		$route['arguments']['__request'] = $this;

		$action_array = explode(":", $route['action']);
		$package_mgr = new PackageMgr( implode("/", array_slice($action_array, 0, 2)) );
		$controller_mgr = $package_mgr->getControllerMgr($action_array[2]);
		return $controller_mgr->fetchAction($action_array[3], $route['arguments']);
	}
	
	/**
	 * Performes request action to a site, with the given variables
	 *
	 * @return \Atom\HttpKernel\Response A response object what is created by the site that was in the Url variable
	 * @author  Daniel Simon
	 */
	function perform() {
	}
	
	/**
	 * Creates an instance of Request class with the variables gathered from globals
	 * Usually used for request actions that're performed by the webbrowser to the server
	 *
	 * @return \Atom\HttpKernel\Request A Request object
	 * @author  Daniel Simon
	 */
	static function createFromGlobals() {
		$request = new Request((array_key_exists("query", $_GET)?$_GET["query"]:"/"), $_POST, $_COOKIE);
		return $request;
	}
	
} // END
