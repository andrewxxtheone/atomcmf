<?php

namespace Atom\Yoo;

use
	Atom\HttpKernel\Response,
	Atom\Twig\FileLoader,
	Atom\Twig\AtomExtension,
	Twig_Environment;

/**
 * Controller super class
 *
 * @package Atom\Yoo
 * @author  Daniel Simon
 */
abstract class Controller {
	
	private $package_mgr;
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author  Daniel Sumon
	 */
	function __construct($package_mgr) {
		$this->package_mgr = $package_mgr;
	}
	
	/**
	 * undocumented function
	 *
	 * @return \Atom\HttpKernel\Response A response object
	 * @author  Daniel Simon
	 */
	function getResponse($file, $data = array()) {
		$loader = new FileLoader(array());
		$loader->addPath($this->package_mgr->getResourceMgr()->getViews(), "Resources");
		$loader->addPath($this->package_mgr->getResourceMgr()->getAssets(), "AsseticResources");
		$loader->addPath("./Resources/Views", "Main");
		$twig = new Twig_Environment($loader, array(
		    'cache' => './Caches/Views',
		));
		$twig->addExtension(new AtomExtension());
		return new Response($twig->render($file, $data)/*, new Headers()*/);
	}
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author  
	 */
	function getEntityMgr() {
	}
	
} // END