<?php

namespace Atom\FileSystem;

/**
 * Dir handler
 *
 * @package Atom\FileSystem
 * @author  Daniel Simon
 */
class Dir {
	
	/**
	 * Path to the dir
	 *
	 * @var string Path to the dir handled by this instance
	 */
	var $dir;
	
	/**
	 * Creates an instance
	 *
	 * @return void
	 * @author  Daniel Simon
	 * @param string Path to the dir
	 */
	function __construct($dir) {
		$this->dir = $dir;
	}
	
	/**
	 * Navigates to a directory
	 *
	 * @return void
	 * @author  Daniel Simon
	 * @param string Navigation to.. (dir name, or . or ..)
	 */
	function navigateTo($navigation, $create = true) {
		switch($navigation) {
			case ".":
			case "..":
				$dir_array = explode("/", $this->dir);
				array_pop($dir_array);
				$this->dir = implode("/", $dir_array);
				break;
			default:
				if($create)
					self::create($navigation);
				$this->dir = $this->dir."/".$navigation;
				break;
		}
	}
	
	/**
	 * Lists the files from a given dir
	 *
	 * @return array An array of File instances representing the files in the given (or instance dir)
	 * @author  Daniel Simon
	 * @param string Path to dir, only if its a static call
	 */
	function getFiles($dir = null) {
		if((isset($this) && get_class($this) == __CLASS__)) { //from instance
			$path = $this->dir.$dir;
		} else { //static
			$path = $dir;
		}
		$_glob = glob($path."/*");
		$files = array();
		foreach($_glob as $_path) {
			if(is_file($_path)) {
				$files[] = new File($_path);
			}
		}
		return $files;
	}
	
	/**
	 * Lists the dirs from a given dir
	 *
	 * @return array An array of Dir instances representing the dirs in the given (or instance dir)
	 * @author  Daniel Simon
	 * @param string Path to dir, only if its a static call
	 */
	function getDirs($dir = null) {
		if((isset($this) && get_class($this) == __CLASS__)) { //from instance
			$path = $this->dir.$dir;
		} else { //static
			$path = $dir;
		}
		$_glob = glob($path."/*");
		$dirs = array();
		foreach($_glob as $_path) {
			if(is_dir($_path)) {
				$dirs[] = new Dir($_path);
			}
		}
		return $dirs;
	}
	
	/**
	 * Lists the files/dirs from a given dir
	 *
	 * @return array An array of File/Dir instances representing the files/dirs in the given (or instance dir)
	 * @author  Daniel Simon
	 * @param string Path to dir, only if its a static call
	 */
	function getAll($dir = null) {
		if((isset($this) && get_class($this) == __CLASS__)) { //from instance
			$path = $this->dir.$dir;
		} else { //static
			$path = $dir;
		}
		$_glob = glob($path."/*");
		$all = array();
		foreach($_glob as $_path) {
			if(is_file($_path)) {
				$all[] = new File($_path);
			} else {
				$all[] = new Dir($_path);
			}
		}
		return $all;
	}
	
	/**
	 * Creates a folder or folder hiearchy
	 *
	 * @return bool True if created
	 * @author  Daniel Simon
	 * @param string Full dir path if its static, only creating dir name from instance
	 */
	function create($dir = null) {
		if((isset($this) && get_class($this) == __CLASS__)) { //from instance
			$path = $this->dir."/".$dir;
		} else { //static
			$path = $dir;
		}
		$path_array = explode("/", $path);
		$creating = null;
		foreach($path_array as $path_part) {
			$creating .= (($creating)?"/":"").$path_part;
			if(!is_dir($creating))
				@mkdir($creating);
		}
	}
	
	/**
	 * Deletes every file and folder from the directory (and itself)
	 *
	 * @return bool True if deleted
	 * @author  Daniel Simon
	 * @param string Full dir path if its static, only creating dir name from instance
	 */
	function delete($dir = null) {
		if((isset($this) && get_class($this) == __CLASS__)) { //from instance
			$dir = $this->dir;
		} else { //static
			$dir = $dir;
		}
		$all = self::getAll($dir);
		foreach($all as $pos)
			$pos->delete();
		@rmdir($dir);
	}
	
	/**
	 * Relative path
	 *
	 * @return string Path of dir
	 * @author  Daniel Simon
	 */
	function getPath() {
		return $this->dir;
	}
	
	/**
	 * Determine dir existance
	 *
	 * @return bool True if dir exists
	 * @author  Daniel Simon
	 */
	function exists() {
		return is_dir($this->dir);
	}
	
} // END