<?php

namespace Atom\FileSystem;

/**
 * File handler
 *
 * @package Atom\FileSystem
 * @author  Daniel Simon
 */
class File {
	
	/**
	 * The path to the file, that this instance handles
	 *
	 * @var string Path to the file
	 */
	var $file_path;
	
	/**
	 * Creates new instance, handles the file, givan as argument
	 *
	 * @return void
	 * @author  Daniel Simon
	 * @param string Path to the to be handled file
	 */
	function __construct($file) {
		$this->file_path = $file;
	}
	
	/**
	 * Creates a file
	 *
	 * @return bool If successfuly created the file
	 * @author  Daniel Simon
	 * @param string Path to the file to be created
	 */
	function create($file_path = null) {
		if((isset($this) && get_class($this) == __CLASS__)) { //from instance
			$path = $this->file_path;
		} else { //static
			$path = $file_path;
		}
		$exploded = explode("/", $path);
		array_pop($exploded);
		$_path = implode("/", $exploded);
		Dir::create($_path);
		file_put_contents($path, "");
	}
	
	/**
	 * Returns the content of the file, definend in the instance constructor if non-static call, or in the argument if static call
	 *
	 * @return string Content of the file
	 * @author  Daniel Simon
	 * @param string Path to the file, only if its a static call
	 */
	function getContent($file_path = null) {
		if((isset($this) && get_class($this) == __CLASS__)) { //from instance
			$path = $this->file_path;
		} else { //static
			$path = $file_path;
		}
		return file_get_contents($path);
	}
	
	/**
	 * Overwrites or creates a file and sets its content to...
	 *
	 * @return bool True if content successfuly overwritten
	 * @author  Daniel Simon
	 * @param string If non-static call, the content of the file, otherwise the path to the file
	 * @param string If non-static call its not used, otherwise the content to be in the file
	 */
	function setContent($x, $y = null) {
		if((isset($this) && get_class($this) == __CLASS__)) { //from instance
			$path = $this->file_path;
			$con = $x;
		} else { //static
			$path = $x;
			$con = $y;
		}
		self::create($path);
		file_put_contents($path, $con);
	}
	
	/**
	 * Deletes the file
	 *
	 * @return bool True if deletes successfuly
	 * @author  Daniel Simon
	 * @param string Path to the file, only if its a static call
	 */
	function delete($file_path = null) {
		if((isset($this) && get_class($this) == __CLASS__)) { //from instance
			$path = $this->file_path;
		} else { //static
			$path = $file_path;
		}
		@unlink($path);
	}
	
	/**
	 * Relative path
	 *
	 * @return string Path of file
	 * @author  Daniel Simon
	 */
	function getPath() {
		return $this->file_path;
	}
	
	/**
	 * Returns file's extension
	 *
	 * @return string File's extension
	 * @author Daniel Simon
	 **/
	function getExtension() {
		$exp = explode(".", $this->file_path);
		return array_pop($exp);
	}
} // END