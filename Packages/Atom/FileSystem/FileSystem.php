<?php

namespace Atom\FileSystem;

/**
 * Class to handle file system
 *
 * @package Atom\FileSystm
 * @author  Daniel Simon
 */
class FileSystem {
	
	/**
	 * Lists the files from a given dir
	 *
	 * @return array An array of File instances representing the files in the given 
	 * @author  Daniel Simon
	 * @param string Path to dir,
	 */
	static function getFiles($dir) {
		return Dir::getFiles($dir);
	}
	
	/**
	 * Lists the dirs from a given dir
	 *
	 * @return array An array of Dir instances representing the dirs in the given
	 * @author  Daniel Simon
	 * @param string Path to dir
	 */
	static function getDirs($dir) {
		return Dir::getDirs($dir);
	}
	
	/**
	 * Lists the files/dirs from a given dir
	 *
	 * @return array An array of File/Dir instances representing the files/dirs in the given
	 * @author  Daniel Simon
	 * @param string Path to dir
	 */
	static function getAll($dir) {
		return Dir::getAll($dir);
	}
	
	/**
	 * Creates a file and fulfill it with content
	 *
	 * @return boolean True if succes Dir create
	 * @author  Daniel Simon
	 * @param string path to the file with file name
	 * @param string content of the file
	 */
	static function createFile($path, $content) {
		return File::setContent($path, $content);
	}
	
	/**
	 * Creates dir recursively if neccesary
	 *
	 * @return boolean True if succes Dir create
	 * @author  Daniel Simon
	 * @param string Path
	 */
	static function createDir($dir) {
		return Dir::create($dir);
	}
	
	/**
	 * Opens the given file
	 *
	 * @return \Atom\FileSystem\File instance
	 * @author  Daniel Simon
	 * @param string Path to the file
	 */
	static function openFile($file) {
		return new File($file);
	}
	
	/**
	 * Opens the given dir
	 *
	 * @return \Atom\FileSystem\Dir instance
	 * @author  Daniel Simon
	 * @param string Path to the dir
	 */
	static function openDir($dir) {
		return new Dir($dir);
	}
	
} // END