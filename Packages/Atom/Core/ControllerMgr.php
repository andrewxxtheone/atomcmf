<?php

namespace Atom\Core;

/**
 * undocumented class
 *
 * @package default
 * @author  
 */
class ControllerMgr extends \ReflectionClass {
	
	private $package_mgr;
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author  Daniel Simon
	 * @param string Controller class with namespace and shit
	 */
	function __construct($controller_class, &$package_mgr) {
		parent::__construct($controller_class);
		$this->package_mgr = $package_mgr;
	}
	
	/**
	 * undocumented function
	 *
	 * @return \Atom\HttpKernel\Response
	 * @author  Daniel Simon
	 * @param string Action's name
	 * @param array Arguments to pass to the action method
	 */
	function fetchAction($action, $arguments) {
		if(!$this->hasMethod($action."Action")) {
			throw new Exception();
		}
		$method_ref = $this->getMethod($action."Action");
		$method_args = $method_ref->getParameters();
		$ordered_args = array();
		foreach($method_args as $ma) {
			if($a = $ma->getClass()) {
				$ordered_args[] = $arguments["__request"];
			} else {
				$ordered_args[] = $arguments[$ma->getName()];
			}
		}
		$co_instance = $this->newInstance($this->package_mgr);
		return $method_ref->invokeArgs($co_instance, $ordered_args);
	}
	
} // END