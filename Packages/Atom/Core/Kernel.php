<?php

namespace Atom\Core;

use Atom\HttpKernel\Request;

/**
 * AtomCMF's core class, called Kernel
 *
 * @package Atom\Core
 * @author  Daniel Simon
 */
abstract class Kernel {
	
	/**
	 * Constructor of Kernel class
	 *
	 * @return void
	 * @author  Daniel Simon
	 */
	function __construct() {
	}
	
	/**
	 * Handles the request performed by webbrowser to server
	 *
	 * @return void
	 * @author  Daniel Simon
	 * @param \Atom\HttpKernel\Request Request object created from action performed by client's webbrowser 
	 */
	function handle(Request $request) {
		$response = $request->handle();
		$response->display();
	}
	
} // END
