<?php

namespace Atom\Core;

/**
 * Package Manager class
 *
 * @package Atom\Core
 * @author  Daniel Simon
 */
class PackageMgr {
	
	/**
	 * Package's path
	 *
	 * @var string Path to the package
	 */
	var $package_path;
	
	/**
	 * Package's name
	 *
	 * @var string Package name
	 */
	var $package_name;
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author  Daniel Simon
	 * @param string Package namespace
	 */
	function __construct($package_path) {
		$this->package_name = $package_path;
		$this->package_path = "./Packages/".$package_path;
	}
	
	/**
	 * undocumented function
	 *
	 * @return /Atom/Core/ResourceMgr A resource manager instace
	 * @author  Daniel Simon
	 */
	function getResourceMgr() {
		return new ResourceMgr($this->package_path."/Resources/");
	}
	
	/**
	 * ???
	 *
	 * @return ???
	 * @author  Daniel Simon
	 */
	function getControllerMgr($controller) {
		return new ControllerMgr(str_replace("/", "\\",$this->package_name)."\Controllers\\".$controller."Controller", $this);
	}
	
	/**
	 * undocumented function
	 *
	 * @return mixed Value of package info's specified key
	 * @author  Daniel Simon
	 * @param string Info's key
	 */
	function getPackageInfo($info_key) {
	}
	
} // END
