<?php

namespace Atom\Core;

use
	Atom\FileSystem\File;

/**
 * Cache Manager static class
 *
 * @package Atom\Core
 * @author  Daniel Simon
 */
class CacheMgr {
	
	/**
	 * Cache dir
	 */
	const Cache_dir = "./Caches/";
	
	/**
	 * Returns true if the cache file exists
	 *
	 * @return bool
	 * @author  Daniel Simon
	 * @param string Cache file name
	 */
	static function is($file) {
		return (bool)file_exists(self::Cache_dir.$file.".php");
	}
	
	/**
	 * Returns the timestamp of the file's last modification
	 *
	 * @return integer Last modification time
	 * @author  Daniel Simon
	 * @param string Cache file name
	 */
	static function lastmtime($file) {
		return filemtime(self::Cache_dir.$file.".php");
	}
	
	/**
	 * Returns the content held by the cache file
	 *
	 * @return mixed Content of the cache file
	 * @author  Daniel Simon
	 * @param string Cache file name
	 */
	static function get($file) {
		return require self::Cache_dir.$file.".php";
	}
	
	/**
	 * Creates a cache file, with specified name and content
	 *
	 * @return bool True if cache file is successfuly created otherwise false
	 * @author  Daniel Simon
	 * @param string Cache file name
	 * @param string Cacge file's content
	 */
	static function create($file, $data) {
		return (bool)File::setContent(self::Cache_dir.$file.".php", "<?php\nreturn ".var_export($data, true).";");
	}
	
	/**
	 * Deletes a cache file
	 *
	 * @return bool True if cache file is successfuly deleted otherwise false
	 * @author  Daniel Simon
	 * @param string Cache file name
	 */
	static function delete($file) {
		@unlink(self::Cache_dir.$file.".php");
		return self::is($file);
	}
	
} // END