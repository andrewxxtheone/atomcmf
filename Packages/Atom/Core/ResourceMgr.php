<?php

namespace Atom\Core;

/**
 * Resource Manager class
 *
 * @package Atom\Core
 * @author  Daniel Simon
 */
class ResourceMgr {
	
	/**
	 * undocumented class variable
	 *
	 * @var string Path to the Resources folder
	 */
	var $resources_path;
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author  
	 */
	function __construct($resources_path = null) {
		if($resources_path)
			$this->resources_path = $resources_path;
		else
			$this->resources_path = "./Resources/";
	}
	
	/**
	 * undocumented function
	 *
	 * @return array An array of settings held by the called config file
	 * @author  Daniel Simon
	 * @param string Config file's name without extension
	 */
	function getConfig($config_name) {
		if(!CacheMgr::is("Configs/".$config_name) || (CacheMgr::lastmtime("Configs/".$config_name) < filemtime($this->resources_path."Configs/".$config_name.".yaml"))) {
			CacheMgr::delete("Configs/".$config_name);
			CacheMgr::create("Configs/".$config_name, \Atom\Yaml\Spyc::YAMLLoad($this->resources_path."Configs/".$config_name.".yaml"));
		}
		return CacheMgr::get("Configs/".$config_name);
	}
	
	/**
	 * ???
	 *
	 * @return ???
	 * @author  Daniel Simon
	 * @param string ???
	 */
	function getAssets() {
		return $this->resources_path."Assets";
	}
	
	/**
	 * undocumented function
	 *
	 * @return array An array of text collections
	 * @author  Daniel Simon
	 * @param string Language name
	 */
	function getLanguage($language) {
	}
	
	/**
	 * ???
	 *
	 * @return ???
	 * @author  Daniel Simon
	 * @param string ???
	 */
	function getViews() {
		return $this->resources_path."Views";
	}
	
} // END
