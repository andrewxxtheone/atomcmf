<?php
namespace Atom\Twig;

use
	Twig_Extension,
	Twig_Environment,
	Atom\Core\ResourceMgr,
	Atom\HttpKernel\Router;

class AtomExtension extends Twig_Extension {
		
	var $env;
	var $system_config;

    public function initRuntime(Twig_Environment $environment) {
    	$this->env = $environment;
    	$resmgr = new ResourceMgr();
    	$this->system_config = $resmgr->getConfig("System");
    }

	public function getTokenParsers() {
		return array(
			// {% render 'BlogBundle:Post:list' with { 'limit': 2 }, { 'alt': 'BlogBundle:Post:error' } %}
			//new RenderTokenParser(),
		);
	}
	
	public function getFunctions() {
		return array(
			"resource" => new \Twig_Function_Method($this, "resource"),
			"route" => new \Twig_Function_Method($this, "route"),
			"trans" => new \Twig_Function_Method($this, "trans"),
			"inlineCSS" => new \Twig_Function_Method($this, "inlineCSS", array('pre_escape' => 'html', 'is_safe' => array('html'))),
			"inlineJS" => new \Twig_Function_Method($this, "inlineJS", array('pre_escape' => 'html', 'is_safe' => array('html'))),
			"CSS" => new \Twig_Function_Method($this, "css", array('pre_escape' => 'html', 'is_safe' => array('html'))),
		);
	}
	
    public function getFilters() {
    	return array(
			//"translate" => new \Twig_Filter_Method($this, "Translate"),
		);
    }
	
	public function Resource($assetic) {
		$real_path = $this->env->getLoader()->getCacheKey(str_replace(array("@Resources"),array("@AsseticResources"), $assetic));
		$real_path_array = explode("/", str_replace("./Packages/", "", $real_path));
		$provider = array_shift($real_path_array);
		$package = array_shift($real_path_array);
		array_shift($real_path_array);
		array_shift($real_path_array);
		$asset_path = implode("/", $real_path_array);
		return $this->system_config["static_url"].$provider.$package."/".$asset_path;
	}

	public function Route($key, $args = array()) {
		return $this->system_config["base_url"].Router::createUrl($key, $args);
	}

	public function Trans($key, $args = array()) {

	}

	public function InlineCSS($assetic) {
		$real_path = $this->env->getLoader()->getCacheKey(str_replace(array("@Resources"),array("@AsseticResources"), $assetic));
		$real_path_array = explode("/", str_replace("./Packages/", "", $real_path));
		$provider = array_shift($real_path_array);
		$package = array_shift($real_path_array);
		array_shift($real_path_array);
		array_shift($real_path_array);
		$asset_path = implode("/", $real_path_array);
		return "<style>\n".file_get_contents("./Packages/".$provider."/".$package."/Resources/Assets/".$asset_path)."\n</style>\n";
	}

	public function InlineJS($assetic) {

	}

	public function CSS($assetic) {
		return "<link type=\"text/css\" rel=\"stylesheet\" href=\"".$this->Resource($assetic)."\" media=\"all\" />\n";
	}
	
	public function getName() {
		return 'atom';
	}
}
