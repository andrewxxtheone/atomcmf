<?php
namespace Atom\CLI;

/**
 * undocumented class
 *
 * @package Atom\CLI
 * @author  Daniel Simon
 */
class Args {
	
	/**
	 * undocumented class variable
	 *
	 * @var string
	 */
	var $flags;
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author  Daniel Simon
	 */
	function __construct($_argv) {
		$args = array();
		array_shift($_argv);
		for($i=0;$i<count($_argv);$i++) {
			if(strpos($_argv[$i], "-") === TRUE) {
				if(!array_key_exists($i+1, $_argv) || strpos($_argv[$i+1], "-") !== FALSE) {
					$args[substr($_argv[$i], 1)] = true;
				} else {
					$args[substr($_argv[$i], 1)] = $_argv[$i+1];
					$i++;
				}
			}else {
				$args["command"] = $_argv[$i];
			}
		}
		$this->flags = $args;
	}
	
	/**
	 * undocumented function
	 *
	 * @return string/integer/boolean
	 * @author  Daniel Simon
	 */
	function flag($flag) {
		return (array_key_exists($flag, $this->flags)?$this->flags[$flag]:false);
	}
	
} // END