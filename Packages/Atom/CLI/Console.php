<?php

namespace Atom\CLI;

/**
 * Console handler class
 *
 * @package Atom\CLI
 * @author  Daniel Simon
 */
class Console {
	
	/**
	 * Reservec commands
	 *
	 * @var array Reserved Commads to CLI
	 */
	var $reserved_commands;
	
	/**
	 * Creates new Console instance
	 *
	 * @return void
	 * @author  Daniel Simon
	 */
	function __construct() {
		
		$this->reserved_commands = array(
			"database" => "\\Atom\\CLI\\Commands\\Database",
			"resources" => "\\Atom\\CLI\\Commands\\Resources"
		);
		
		$args = new Args($_SERVER["argv"]);
		$this->run($args);
		/*var_dump($args->flag("sql"));
		var_dump($args->flag("model"));
		var_dump($args->flag("overwrite"));*/
		
		/*echo "Command: ".$args->flag("command")." params:\n";
		var_dump($args->flag("sql"));
		var_dump($args->flag("model"));
		var_dump($args->flag("overwrite"));*/
	}
	
	/**
	 * Handles commands
	 * Command call example:
	 * 		resources:publish_assets
	 * 		database:compile -sql -model -overwrite
	 * 		database:import
	 * 		acme:demo:some_other_command -param1 value -param2 value -param3 value
	 *
	 * @return void
	 * @author  Daniel Simon
	 */
	function run($args) {
		if($args->flag("command")) { //execute
			$this->executeCommand($args);
		} else {
			echo "ERROR: No command passed!";
		}
	}
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author  
	 */
	function executeCommand($args) {
		$command_array = explode(":", $args->flag("command"));
		if(array_key_exists($command_array[0], $this->reserved_commands)) {
			$command_object_ref = new \ReflectionClass($this->reserved_commands[$command_array[0]]);
			$command_object = $command_object_ref->newInstance();
			$command_object->execute($args);
		} else {
			/*
			 * vendor:package:command
			 * acme:site:update
			 */
			$command_object_ref = new \ReflectionClass($this->reserved_commands[$command_array[0]]);
			$command_object = $command_object_ref->newInstance();
			$command_object->execute($args);
		}
	}
} // END
