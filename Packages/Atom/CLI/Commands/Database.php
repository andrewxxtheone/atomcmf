<?php

namespace Atom\CLI\Commands;

use
	Atom\FileSystem\FileSystem as FS;

/**
 * undocumented class
 *
 * @package default
 * @author  
 */
class Database {
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author  
	 */
	function execute($args) {
		echo "Command: ".$args->flag("command")." params:\n";
		/*var_dump($args->flag("sql"));
		var_dump($args->flag("model"));
		var_dump($args->flag("overwrite"));*/
		$vendors = FS::getDirs("./Packages");
		foreach($vendors as $vendor) {
			$packages = $vendor->getDirs();
			foreach($packages as $package) {
				$package->navigateTo("Resources/Assets", false);
				if($package->exists())
					echo realpath($package->getPath())."\n";
			}
		}
		
	}
} // END