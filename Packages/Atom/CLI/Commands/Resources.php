<?php

namespace Atom\CLI\Commands;

use
	Atom\CLI\Command,
	Atom\Core\PackageMgr,
	Atom\FileSystem\File,
	Atom\FileSystem\Dir,
	Atom\FileSystem\FileSystem as FS;

/**
 * Resource handler command
 *
 * @package Atom\CLI\Commands
 * @author  Daniel Simon
 */
class Resources extends Command {
	
	/**
	 * undocumented function
	 *
	 * @return void
	 * @author  
	 */
	function execute($args) {

		if($args->flag("command") == "resources:publish")
			$this->publishAssets();
		if($args->flag("command") == "resources:parse-sass")
			$this->parseSass();
	}

	/**
	 * Creates symlinks or htaccess file to redirect static assets files
	 *
	 * @return void
	 * @author Daniel Simon
	 **/
	function publishAssets() {
		FS::openDir("./static")->delete();
		FS::createDir("./static");
		$vendors = FS::getDirs("./Packages");
		foreach($vendors as $vendor) {
			$packages = $vendor->getDirs();
			foreach($packages as $package) {
				$resources = FS::openDir($package->getPath());
				$resources->navigateTo("Resources/Assets", false);
				if($resources->exists()) {
					$vendor_explode = explode("/", $vendor->getPath());
					$package_explode = explode("/", $package->getPath());
					echo realpath("./static/")."\\".$vendor_explode[count($vendor_explode)-1].$package_explode[count($package_explode)-1]."\n";
					echo realpath($resources->getPath())."\n";
					symlink(realpath($resources->getPath()), realpath("./static/")."\\".$vendor_explode[count($vendor_explode)-1].$package_explode[count($package_explode)-1]);
				}
			}
		}
	}

	/**
	 * Parse Sass files to Css file
	 *
	 * @return void
	 * @author Daniel Simon
	 **/
	function parseSass() {
		$vendors = FS::getDirs("./Packages");
		$paths = array();
		foreach($vendors as $vendor) {
			$packages = $vendor->getDirs();
			foreach($packages as $package) {
				$package_mgr = new PackageMgr(str_replace("./Packages/", "", $package->getPath()));
				$assets_path = $package_mgr->getResourceMgr()->getAssets();
				$assets = FS::openDir($assets_path);
				$this->discoverDirectoriesInAssets($assets, $paths);
			}
		}
	    $sassParserOptions = array(
			//'basepath' => null,
			'debug_info' => FALSE,
			'filename' => array('dirname' => '', 'basename' => ''),
			'functions' => array(
				"url" => function($a) {
					echo $a;
					return "b";
				}
			),
			'load_paths' => array(
				"./Resources/Assets/bourbon/"
			),
			'load_path_functions' => array(),
			'line' => 1,
			'line_numbers' => FALSE,
			'style' => \SassRenderer::STYLE_NESTED,
			'syntax' => \SassFile::SCSS,
			'debug' => FALSE,
			'quiet' => FALSE,
			'callbacks' => array(
				'warn' => function($a) {echo $a.'\n';},
				'debug' => function($a) {echo $a.'\n';},
	      	),
    	);
    	$parser = new \SassParser($sassParserOptions);
		foreach($paths as $path) {
			echo $path["instance"]->getPath()."\n";
			$files = $path["instance"]->getFiles();
			foreach($files as $file) {
				if($file->getExtension() == "scss") {
					File::delete(str_replace(".scss", ".css", $file->getPath()));
					FS::createFile(str_replace(".scss", ".css", $file->getPath()), $parser->toCss($file->getPath()));
				}
			}
		}
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function discoverDirectoriesInAssets(Dir $dir, &$paths) {
		if(!$dir->exists())
			return;
		if(!in_array($dir, $paths)) 
			$paths[] = array(
					"instance" => $dir,
					"path" => $dir->getPath()
				);
		$subdirs = $dir->getDirs();
		foreach($subdirs as $subdir)
			$this->discoverDirectoriesInAssets($subdir, $paths);
	}
} // END