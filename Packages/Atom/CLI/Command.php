<?php
namespace Atom\CLI;

/**
 * Command base class
 *
 * @package Atom\CLI
 * @author  Daniel Simon
 */
abstract class Command {
	
	/**
	 * Writes a text as an output
	 *
	 * @return void
	 * @author  Daniel Simon
	 */
	function write($text) {
	}
	
	/**
	 * Asks user to write something
	 *
	 * @return string User typed text
	 * @author  Daniel Simon
	 */
	function read($text = null) {
	}
	
} // END