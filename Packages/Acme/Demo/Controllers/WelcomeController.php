<?php

namespace Acme\Demo\Controllers;

use
	Atom\Yoo\Controller;

class WelcomeController extends Controller {
	
	function indexAction($name) {
		return $this->getResponse("@Resources/helloworld.html", array("name" => $name));
	}
	
}
